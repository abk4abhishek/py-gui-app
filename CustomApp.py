import sys
from PyQt5.QtWidgets import QApplication, QWidget,QPushButton, QMainWindow, QLineEdit, QMessageBox, QTableWidget,QTableWidgetItem
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot
 
class App(QMainWindow):
 
    def __init__(self):
        super().__init__()

        #  App Properties 
        self.title = 'BDD Manager'
        self.left = 0
        self.top = 0
        self.width = 800
        self.height = 600
        self.statusMessage = 'Running - OK, No Pending task'

        #  Initialize App 
        self.initUI()
 
    def initUI(self):
        
        # ----    App Elements  ------ #

        # Button
        button = QPushButton('Add Step', self)
        button.setToolTip('To add new step in step directory')
        button.move(100,70) 
        button.clicked.connect(self.on_click) 

        # Text box
        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(280,40)
        self.textbox.setGeometry(40,40,600,20)
        

        #  Set App Properties 
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        # Set Status bar
        self.statusBar_UpdateMessage()

        # Render App 
        self.show()


    @pyqtSlot()
    def on_click(self):
        print('TBD - Please come back again')
        self.statusMessage = 'Running - OK, No Pending task'+': TBD - Please come back Again'
        self.statusBar_UpdateMessage()

    def statusBar_UpdateMessage(self):
        self.statusBar().showMessage(self.statusMessage)
        textboxValue = self.textbox.text()
        QMessageBox.question(self, 'BD - ?', "You typed: " + textboxValue, QMessageBox.Ok, QMessageBox.Ok)
        self.textbox.setText("")

 
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())