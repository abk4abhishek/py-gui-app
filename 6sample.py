
"""
ZetCode PyQt5 tutorial 

In this example, we create a bit
more complicated window layout using
the QGridLayout manager. 

author: Jan Bodnar
website: zetcode.com 
last edited: January 2015
"""

import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import (
    QWidget, 
    QLabel, 
    QLineEdit, 
    QTextEdit,
    QPushButton, 
    QGridLayout,
    QTableWidget,
    QTableWidgetItem, 
    QApplication)


class Example(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.initUI()
        
        
    def initUI(self):
        
        TestSteps = [
            {"Module":"Homepage","Step":"Given User is on Home page","TestData":"QA,Stage,Prod"},
            {"Module":"WebResult","Step":"Given User is on Web Result page","TestData":"QA,Stage,Prod"},
            {"Module":"ImageResult","Step":"Given User is on Image Result page","TestData":"QA,Stage,Prod"},
            {"Module":"VideoResult","Step":"Given User is on Video Result page","TestData":"QA,Stage,Prod"}
        ]

        title = QLabel('Module')
        step = QLabel('Step')
        stepdata = QLabel('Test Data')

        titleEdit = QLineEdit()
        stepEdit = QLineEdit()
        stepdataEdit = QTextEdit()

        addbutton = QPushButton("Add")
        

        grid = QGridLayout()
        grid.setSpacing(10)

        grid.addWidget(title, 1, 0)
        grid.addWidget(titleEdit, 1, 1,1,3)

        grid.addWidget(step, 2, 0)
        grid.addWidget(stepEdit, 2, 1,1,3)

        
        grid.addWidget(stepdata, 3, 0)
        grid.addWidget(stepdataEdit, 3, 1, 5, 3)
        grid.addWidget(addbutton, 9,1,1,1)
        
        stepstable = QTableWidget()
        stepstable.setRowCount(4)
        stepstable.setColumnCount(3)
        stepstable.setHorizontalHeaderLabels(("Module;Step;Test Data").split(";"))
        # table.setVerticalHeaderLabels(QString("V1;V2;V3;V4").split(";"))
        stepstable.setColumnWidth(0, 150)
        stepstable.setColumnWidth(1, 600)
        stepstable.setColumnWidth(2, 300)

        stepstable.horizontalHeader().setSortIndicatorShown(True)
        c = 0
        for row in TestSteps:
                    stepstable.setItem(c, 0, QTableWidgetItem(row["Module"]))
                    stepstable.setItem(c, 1, QTableWidgetItem(row["Step"]))
                    stepstable.setItem(c, 2, QTableWidgetItem(row["TestData"]))
                    c = c + 1

        # stepstable.setItem(0,0,QTableWidgetItem("Module"))
        # stepstable.setItem(0,1,QTableWidgetItem("Step"))
        # stepstable.setItem(0,2,QTableWidgetItem("Test Data"))
        # stepstable.setItem(1,0,QTableWidgetItem("Module"))
        # stepstable.setItem(1,1,QTableWidgetItem("Step"))
        # stepstable.setItem(1,2,QTableWidgetItem("Test Data"))
        # stepstable.setItem(2,0,QTableWidgetItem("Module"))
        # stepstable.setItem(2,1,QTableWidgetItem("Step"))
        # stepstable.setItem(2,2,QTableWidgetItem("Test Data"))
        # stepstable.setItem(3,0,QTableWidgetItem("Module"))
        # stepstable.setItem(3,1,QTableWidgetItem("Step"))
        # stepstable.setItem(3,2,QTableWidgetItem("Test Data"))
        # stepstable.setItem(4,0,QTableWidgetItem("Module"))
        # stepstable.setItem(4,1,QTableWidgetItem("Step"))
        # stepstable.setItem(4,2,QTableWidgetItem("Test Data"))

        grid.addWidget(stepstable, 10,0,5,4)

        self.setLayout(grid) 
        
        self.setGeometry(50, 50, 1200, 800)
        self.setWindowTitle('BDD Manager')    
        self.show()
        
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())